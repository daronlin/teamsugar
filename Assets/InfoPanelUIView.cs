﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;
using System.Linq;
using UnityEngine.UI;

public class InfoPanelUIView : MonoBehaviour {

	public Flowchart fc;
	public List<string> dataKey;

	public List<InfoData> InfoData; 

	public GameObject DataUI;

	public GameObject DataLayout;


	public List<GameObject> DataUIList;

	public Button BtnShowUI;
	public GameObject DataInfoAllUI;

	// Use this for initialization
	void Start () {
		dataKey = fc.GetVariableNames ().ToList ();

		DataUIList = new List<GameObject> ();
		DataUIList.Add (DataUI);
		SetUIBtn ();

		BtnShowUI.onClick.AddListener (ShowUI);
	}
	
	// Update is called once per frame
	void Update () {
//		SetValue ();
	}

	void SetValue()
	{
		for (int i = 0; i < InfoData.Count; i++) {
			InfoData[i].Value = fc.GetVariable (InfoData [i].Key).ToString();

			DataUIList [i].transform.Find ("Name").GetComponent<Text>().text = InfoData[i].Name;

			DataUIList [i].transform.Find ("Value").GetComponent<Text>().text = InfoData[i].Value.ToString();

		}
	}

	void SetUIBtn()
	{
		for (int i = 0; i < InfoData.Count; i++) {
			GameObject obj;
			if (DataLayout.transform.childCount < (i + 1)) {
				obj = GameObject.Instantiate (DataUI, DataLayout.transform);
				DataUIList.Add (obj);
			}
			else
			{
				obj = DataUIList [i];
			}
		}

		SetValue ();
	}

	void ShowUI()
	{
		DataInfoAllUI.SetActive (!DataInfoAllUI.activeInHierarchy);

		if (DataInfoAllUI.activeInHierarchy) {
			SetValue ();
		}
	}
}

[System.Serializable]
public class InfoData
{

	public string Name = "";
	public string Key = "";
	public string Value = "";
}
