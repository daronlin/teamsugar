﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;
using System;
using System.IO;
using System.Linq;
using UnityEngine.UI;

public class GetFlowData : MonoBehaviour {

	public Flowchart _fc;
	public List<string> dataKey;
	public string StartTime;
	public InputField _nameInputField;

	// Use this for initialization
	void Start () {
		StartTime = DateTime.Now.ToString("yyyy_MM_dd_HH:mm:ss");
//		_fc.SetVariable<string>("RecordTime", StartTime);
		_fc.SetStringVariable ("RecordTime", StartTime);

		dataKey = _fc.GetVariableNames ().ToList ();
	}

	public void SetName()
	{
		_fc.SetStringVariable ("PlayerName", _nameInputField.text);
	}

	public void SetData()
	{
		string log = "";
		for (int i = 0; i < dataKey.Count; i++) {
			//				Debug.Log (dataKey [i]);
			log += dataKey [i];
			if (i != dataKey.Count - 1) {
				log+=",";
			}else
			{
				log+="\n";
			}
		}

		for (int i = 0; i < dataKey.Count; i++) {
			//				Debug.Log (dataKey [i] + _fc.GetVariable (dataKey [i]));

			log += _fc.GetVariable (dataKey [i]);
			if (i != dataKey.Count - 1) {
				log+=",";
			}else
			{
				//					log+="\n";
			}
		}
		CreateFile (log, "Filedata");
	}

	public void CreateFile(string versionCheck,string FileName)
	{
		//		Debug.Log ("CreateFile");

//		string path = Application.persistentDataPath + "/" + FileName + ".csv";
		string path = Application.temporaryCachePath + "/" + FileName + ".txt";
		Debug.Log (path);
		//文件流信息
		StreamWriter sw;
		FileInfo t = new FileInfo (path);
		if(!t.Exists)
		{
			//如果此文件不存在则创建
			sw = t.CreateText();
			Debug.Log ("CreateFile not folder in : "  + path);
		}
		else
		{
			//如果此文件存在则打开
			sw = t.AppendText();
			Debug.Log ("CreateFile Have folder in : "  + path);
			//			sw =
		}
		//以行的形式写入信息
		sw.WriteLine(versionCheck);
		//关闭流
		sw.Close();
		//销毁流
		sw.Dispose();
	}  

	public string LoadFile()
	{
		string path = Application.temporaryCachePath + "/Check.txt";
		//使用流的形式读取
		StreamReader sr =null;
		try{
			sr = File.OpenText(path);
		}catch(Exception e)
		{
			Debug.Log (e.ToString ());
			//路径与名称未找到文件则直接返回空
			Debug.Log(" LoadFile Not Find File : " + path);
			return "";
		}
		string line;
		line = sr.ReadToEnd ();
		//		ArrayList arrlist = new ArrayList();
		//		while ((line = sr.ReadLine()) != null)
		//		{
		//			//一行一行的读取
		//			//将每一行的内容存入数组链表容器中
		//			//			arrlist.Add(line);
		//			Debug.Log("Line : " + line);
		//		}


		//关闭流
		sr.Close();
		//销毁流
		sr.Dispose();
		//将数组链表容器返回
		//		return arrlist;

		return line;
	}
}
